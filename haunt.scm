(use-modules (haunt builder blog)
             (haunt builder assets)
	     (haunt post)
             (haunt html)
             (haunt reader)
             (haunt reader commonmark)
             (haunt site)
             (haunt utils)
             (commonmark)
             (syntax-highlight)
             (syntax-highlight javascript)
             (syntax-highlight scheme)
             (sxml match)
             (sxml simple)
	     (sxml transform)
             (ice-9 match)
             (ice-9 pretty-print)
	     (template)
	     (atom))

;;;  reader that highlights code
(define (maybe-highlight-code lang source)
  (let ((lexer (match lang
                 ('javascript lex-javascript)
		 ('lisp lex-scheme)
                 (_ #f))))
    (if lexer
        (highlights->sxml (highlight lexer source))
        source)))

(define (html-to-sxml source)
  (cdr (xml->sxml source)))

(define (highlight-code . tree)
  (sxml-match tree
   ((pre (code (@ (class ,class) . ,attrs) ,source))
    (let ((lang (string->symbol
		 (string-drop class (string-length "language-")))))
      (if (eq? lang 'html)
	  ;; strip the <pre> from the html
  (html-to-sxml source)
	  `(pre (code (@ ,@attrs)
		 ,(maybe-highlight-code lang source))))))
   (,other other)))

(define (sxml-identity . args) args)

(define %commonmark-rules
  `((pre . ,highlight-code)
    (*text* . ,(lambda (tag str) str))
    (*default* . ,sxml-identity)))

(define (post-process-commonmark sxml)
  (pre-post-order sxml %commonmark-rules))

(define commonmark-reader*
  (make-reader (make-file-extension-matcher "md")
               (lambda (file)
                 (call-with-input-file file
                   (lambda (port)
                     (values (read-metadata-headers port)
                             (post-process-commonmark
                              (commonmark->sxml port))))))))

(site #:title "null_radix"
      #:domain "nullradix.eth.link"
      #:default-metadata
      '((author . "mjbecze"))
      #:readers (list commonmark-reader*)
      #:builders (list (blog   #:theme wanderer-theme)
                       (atom-feed)
                       ;; (atom-feeds-by-tag)
		       ;; about-page
		       (static-pages)
               (static-directory ".well-known")
		       (static-directory "assests")
		       (static-directory "css")
		       (static-directory "images")))
