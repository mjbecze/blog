title: About
---

## This site
This site was made with [haunt](https://dthompson.us/manuals/haunt/). You can find the code [here](https://gitlab.com/mjbecze/blog).

### Assets
This site use the following assets.
* svg from [rvklein.me](https://rvklein.me/)
* css from [awsm.css](https://igoradamenko.github.io/awsm.css/)

## My Links
* [code](https://gitlab.com/mjbecze)
* [music im listening to](https://listenbrainz.org/user/null_radix)
* [openhumans](https://www.openhumans.org/member/wanderer/)
* [openstreetmap](https://www.openstreetmap.org/user/null_radix)
* [mastodon](https://koyu.space/@null_radix)
* [email](mailto:null_radix@riseup.net)
* [public key](assests/null_radix%20null_radix@riseup.net%20(0x321A9B0B363B3D97)%20pub.asc)

## Books I have read
* 2020-08-20 - [How Emotions are Made](https://lisafeldmanbarrett.com/books/how-emotions-are-made/)

## [What is Null](https://wiki.c2.com/?WhatIsNull)?
* Null is how much bureaucracy a system needs on top of "nothing".
* Null is the alpha and the omega, everything begins as null, is initialized, then reset and reused as Null.
* The Null that can be named is not the true Null.
* There are known knowns. These are things we know that we know. There are known unknowns. That is to say, there are things that we know we don't know. But there are also unknown unknowns. There are things we don't know we don't know. - Donald Rumsfeld on Null
