(define-module (atom)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt html)
  #:use-module (srfi srfi-19) 		;; date->string
  #:use-module (srfi srfi-26) 		;; cut
  #:use-module (haunt utils)
  #:use-module (sxml simple)
  #:use-module (template)
  #:export (atom-feed))


;;;
;;; Atom feed (stolen from guix-hpc (stolen from Haunt and adjusted)).
;;;

;;; We cannot use Haunt's 'atom-feed' because of the non-default post URLs
;;; that we use.  Thus the code below is mostly duplicated from (haunt
;;; builder atom), with the exception of the URLs.

(define (sxml->xml* sxml port)
  "Write SXML to PORT, preceded by an <?xml> tag."
  (display "<?xml version=\"1.0\" encoding=\"utf-8\"?>" port)
  (sxml->xml sxml port))

(define (date->string* date)
  "Convert date to ISO-8601 formatted string."
  (date->string date "~4"))

(define* (post->atom-entry site post #:key (blog-prefix ""))
  "Convert POST into an Atom <entry> XML node."
  `(entry
    (title ,(post-ref post 'title))
    (author
     (name ,(post-ref post 'author))
     ,(let ((email (post-ref post 'email)))
        (if email `(email ,email) '())))
    (updated ,(date->string* (post-date post)))
    (link (@ (href ,(string-append blog-prefix (post-uri site post )))
             (rel "alternate")))
    (summary (@ (type "html"))
             ,(sxml->html-string (post-sxml post)))))

(define* (atom-feed #:key
                    (file-name "feed.xml")
                    (subtitle "Recent Posts")
                    (filter posts/reverse-chronological)
                    (max-entries 20)
                    (blog-prefix ""))
  "Return a builder procedure that renders a list of posts as an Atom
feed.  All arguments are optional:

FILE-NAME: The page file name
SUBTITLE: The feed subtitle
FILTER: The procedure called to manipulate the posts list before rendering
MAX-ENTRIES: The maximum number of posts to render in the feed"
  (lambda (site posts)
    (make-page file-name
               `(feed (@ (xmlns "http://www.w3.org/2005/Atom"))
                      (title ,(site-title site))
                      (subtitle ,subtitle)
                      (updated ,(date->string* (current-date)))
                      (link (@ (href ,(string-append blog-prefix
                                                     file-name))
                               (rel "self")))
                      (link (@ (href ,(site-domain site))))
                      ,@(map (cut post->atom-entry site <>
                                  #:blog-prefix blog-prefix)
                             (take-up-to max-entries (filter posts))))
               sxml->xml*)))
