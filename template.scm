(define-module (template)
  #:use-module (haunt builder blog)
  #:use-module (haunt site)
  #:use-module (haunt post)
  #:use-module (haunt page)
  #:use-module (haunt html)
  #:use-module (haunt reader)
  #:use-module (haunt reader commonmark)
  #:use-module (srfi srfi-19) 		;; date->string
  #:use-module (srfi srfi-11) 		;; let-values
  #:use-module (sxml match)
  #:use-module (sxml simple)
  #:export (wanderer-theme
	    static-pages
	    post-uri))

(define (date->string* date)
  "Convert date to RFC-3339 formatted string."
  (date->string date "~d-~b-~Y"))

(define (stylesheet name)
  `(link (@ (rel "stylesheet")
            (href ,(string-append "css/" name ".css")))))

;;  navbar
(define (section-type body)
  (sxml-match body
	      [(list (header (@ (section ,s) . ,_) . ,a) . ,b)
	       s]
	      [,_ "not"]))

(define (navbar section)
  (define (make-link section)
    (let ((l (cond ((string=? section "blog") "index.html")
		   ((string=? section "feed") "feed.xml")
		   (else (string-append section ".html")))))
      `(a (@ (href ,l)) ,section)))

  (sxml-match '("blog" "about" "feed")
	      [(list ,[i] ...)
	       `(nav
		 (@ (id "main-nav"))
		 (ul
		  (li ,@i) ...))]
	      [,s (guard
		      (string=? s section))
		  `((@ (id "selected-section")) ,(make-link s))]
	      [,a `(,(make-link a))]))

;;  main layout
(define (wanderer-layout site title body)
  `((doctype "html")
    (head
     (meta (@ (charset "utf-8")))
     (meta (@ (name "viewport") (content "width=device-width, initial-scale=1.0")))
     (title ,(string-append title " — " (site-title site)))
     ,(stylesheet "wanderer")
     (link (@ (rel "icon")
	      (href "images/background.svg")
	      (type "image/svg+xml"))))
    (body
     (header (@ (class "no-margin"))
	     (div (@ (class "no-margin")
		     (id "site-title"))
		  (h1
		   ,(site-title site))
		  ,(navbar (section-type body))))
     (main
      (article ,body))
     (footer))))

(define* (post-header title section #:optional author date)
  `(header (@ (section ,section)
	      (class "no-margin"))
	  (h2 (@ (class "no-margin")
		 (id "post-title"))
	      ,title)
	  ,@(if author
		`((h3 (@ (class "no-margin")
			(id "post-author"))
		     (p
		      "by " ,author
		      " " ,date)))
		'())))

(define (wanderer-post-template post)
  (define (comment-link number)
    `(a (@
	(href
	 ,(string-append
	  "https://gitlab.com/mjbecze/blog/-/issues/" number "/")))
       "comments/corrections?"))

  `(,(post-header
      (post-ref post 'title)
      "post"
      (post-ref post 'author)
      (date->string* (post-date post))) 
    (div ,(post-sxml post))
    (h3 (@ (id "comment")) ,(comment-link (post-ref post 'number)))))

(define (post-uri site post)
  (string-append "./" (site-post-slug site post) ".html"))

(define (wanderer-collection-template site title posts prefix)
  `(,(post-header "Recent Posts" "blog")
    (div
     (table
      ,@(map (lambda (post)
	       (define date (post-date post))
	       `(tr
		 (td
		  (time
		   (@ (datetime ,(date->string date
					       "~Y-~m-~dT~H:~M:~SZ")))
		   ,(date->string* date)))
		 (td (a (@ (href ,(post-uri site post)))
			,(post-ref post 'title)))))
	     posts)))))

(define wanderer-theme
  (theme #:name "Wanderer"
	 #:layout wanderer-layout
	 #:post-template wanderer-post-template
	 #:collection-template wanderer-collection-template))

;;;
;;; Static pages.
;;;

(define %cwd
  (and=> (assq-ref (current-source-location) 'filename)
         dirname))

(define read-markdown
  (reader-proc commonmark-reader))

(define (read-markdown-page title page site)
  "Read the CommonMark page from FILE.  Return its final SXML
representation."
  (let-values (((meta body)
                (read-markdown (string-append %cwd "/" (string-append page ".md")))))

    (wanderer-layout site page
		     (list
		      (post-header title page)
		      body))))

(define (static-pages)
  (define (markdown-page title page)
    (lambda (site posts)
      (make-page
       (string-append page ".html")
       (read-markdown-page title page site)
       sxml->html)))

  (markdown-page "About" "about"))
