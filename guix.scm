;;; This module is part of Guix-HPC and is licensed under the same terms,
;;; those of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2017, 2019 Inria

;; Run 'guix build -f guix.scm' to build the web site.
(use-modules (guix)
	     (gnu)
             (guix modules)
	     (guix build-system guile)
	     (gnu packages guile-xyz)
             ((gnu packages guile) #:select (guile-3.0))
             ((guix licenses) #:prefix license:)
             (guix git-download))

(define guile3.0-syntax-highlight-javascript
  (package
    (name "guile-syntax-highlight-javascript")
    (version "0.0.2")
    (source (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/mjbecze/guile-syntax-highlight-javascript.git")
               (commit "bd7c2bc7a80d8a2d42056f79587954c96e61c3d9")))
         (sha256
          (base32
           "04jxaa2ngxipy56pwnwiby5hsm9i6n5dazhqff3iq1ii40fp4pdz"))
         (file-name (git-file-name name version))))
    (build-system guile-build-system)
    (propagated-inputs
     `(("guile-syntax-highlight" ,guile3.0-syntax-highlight)))
    (inputs
     `(("guile" ,guile-3.0)))
    (home-page "https://ngyro.com/software/guile-semver.html")
    (synopsis "Semantic Versioning (SemVer) for Guile")
    (description "This Guile library provides tools for reading,
comparing, and writing Semantic Versions.  It also includes ranges in
the style of the Node Package Manager (NPM).")
    (license license:gpl3+)))

(define haunt
  (specification->package "haunt"))

(define guile-commonmark
  (specification->package "guile-commonmark"))

(define guile-syntax-highlight
  (specification->package "guile-syntax-highlight"))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory "haunt-nullradix"
              #:recursive? #t
              #:select? (git-predicate this-directory)))

(define build
  (with-imported-modules (source-module-closure
                          '((guix build utils)
			    ((gnu packages guile) #:select (guile-3.0))))
    #~(begin
        (use-modules (guix build utils))
        (copy-recursively #$source ".")

        ;; For Haunt.
        (setenv "GUILE_LOAD_PATH"
                (string-append
                 #+(file-append guile3.0-syntax-highlight-javascript
                                "/share/guile/site/3.0")
                 ":"
                 #+(file-append guile-commonmark
                                "/share/guile/site/3.0")
                 ":"
                 #+(file-append guile-syntax-highlight
                                "/share/guile/site/3.0")))

        ;; So we can read/write UTF-8 files.
        (setenv "GUIX_LOCPATH"
                #+(file-append (specification->package "glibc-utf8-locales")
                               "/lib/locale"))
        (setenv "LC_ALL" "en_US.utf8")

        (and (zero? (system* #+(file-append haunt "/bin/haunt")
                             "build"))
             (begin
               (mkdir-p #$output)
               (copy-recursively "site" #$output)))
	)))

(computed-file "haunt-nullradix-web-site" build)
