{
  description = "A flake for building a Haunt static site with Guile";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    guileSyntaxHighlight = {
      url = "git+https://git.dthompson.us/guile-syntax-highlight.git";
      flake = false;
    };
    guile3SyntaxHighlightJavaScript = {
      url = "gitlab:mjbecze/guile-syntax-highlight-javascript";
      flake = false;
    };
  };

  outputs = { nixpkgs, flake-utils, guileSyntaxHighlight, guile3SyntaxHighlightJavaScript, ... }:
    flake-utils.lib.eachDefaultSystem (system:
        let
        pkgs = nixpkgs.legacyPackages.${system};
        # Define custom package for guile-syntax-highlight-javascript
        guile-syntax-highlight = with pkgs; stdenv.mkDerivation rec {
          pname = "guile-syntax-highlight";
          version = "0.11.0";
          nativeBuildInputs = [
            autoreconfHook
            pkg-config
          ];
          buildInputs = [
            guile
          ];
          src =  guileSyntaxHighlight;
        };

        guile-syntax-highlight-javascript = with pkgs; stdenv.mkDerivation rec {
          pname = "guile-syntax-highlight-javascript";
          version = "0.11.0";
          src =  guile3SyntaxHighlightJavaScript;
          installPhase = ''
            mkdir -p $out
            cp -r $src/* $out
          '';
           setupHook = ./setupHook.sh;
        };

        buildInputs = [ pkgs.guile haunt guile-commonmark guile-syntax-highlight guile-syntax-highlight-javascript];

        inherit (pkgs) haunt guile-commonmark;

        in
        {
          packages.default = pkgs.stdenv.mkDerivation {
            name = "haunt-nullradix-web-site";
            src = ./.;
            inherit buildInputs;

            buildPhase = ''
              ${haunt}/bin/haunt build
              '';

            installPhase = ''
              mkdir -p $out
              cp -r site $out/
              '';
          };
          # Development shell
          devShells.default = pkgs.mkShell {
            inherit buildInputs;
          };
        }
  );
}
